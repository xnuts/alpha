//
//  ALPHACore.h
//  Alpha
//
//  Created by Dal Rupnik on 17/06/15.
//  Copyright (c) 2015 Unified Sense. All rights reserved.
//

#import "NSString+Entities.h"
#import "NSString+Identifier.h"
#import "UIApplication+Delegate.h"
#import "UIApplication+Event.h"
#import "UIColor+Random.h"

#import "ALPHAApplicationDelegate.h"
#import "ALPHACanvasView.h"
#import "ALPHAHeapEnumerator.h"
#import "ALPHAMockObject.h"
#import "ALPHARuntimeUtility.h"
#import "ALPHAUtility.h"
#import "ALPHAViewController.h"

