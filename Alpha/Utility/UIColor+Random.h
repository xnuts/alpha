//
//  UIColor+Random.h
//  Alpha
//
//  Created by Dal Rupnik on 17/06/15.
//  Copyright (c) 2015 Unified Sense. All rights reserved.
//

@import UIKit;

@interface UIColor (Random)

+ (UIColor *)alpha_consistentRandomColorForObject:(id)object;

@end
